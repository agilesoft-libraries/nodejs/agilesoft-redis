export interface SaveData {
    key: string;
    data: string;
    expirationSeconds?: number;
}
export declare const createRedisInstance: (connectionString: string, listenAllEvents?: boolean) => void;
export declare const getData: (key: string) => Promise<string | null>;
export declare const saveData: (saveConfig: SaveData) => Promise<void>;
export declare const deleteData: (key: string) => Promise<void>;
