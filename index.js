"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteData = exports.saveData = exports.getData = exports.createRedisInstance = void 0;
const ioredis_1 = __importDefault(require("ioredis"));
const loggerUtil = __importStar(require("agilesoft-logger/logger"));
const logger = loggerUtil.createInstance('agilesoft-redis');
let redisInstance = null;
const parseConnectionString = (connectionString) => {
    logger.info(`Se procesa string de conexión`);
    let nodesConfig;
    try {
        nodesConfig = JSON.parse(connectionString);
    }
    catch (err) {
        const buff = Buffer.from(connectionString, 'base64').toString();
        nodesConfig = JSON.parse(buff);
    }
    return nodesConfig;
};
// const redis = new Redis();
const createRedisInstance = (connectionString, listenAllEvents = false) => {
    if (redisInstance) {
        return;
    }
    logger.info(`Se inicializa proceso de conexión a servidor Redis`);
    const nodesConfig = parseConnectionString(connectionString);
    try {
        if (Array.isArray(nodesConfig)) {
            logger.info('Se intenta conectar con Cluster Redis');
            redisInstance = new ioredis_1.default.Cluster(nodesConfig);
        }
        else {
            logger.info('Se intenta conectar con Cluster Redis');
            redisInstance = new ioredis_1.default(nodesConfig);
        }
        redisInstance.on('ready', () => {
            logger.info('Conexión con Redis establecida');
            if (listenAllEvents) {
                logger.info('Activando configuracion para escuchar eventos de expiracion de objetos notify-keyspace-events');
                redisInstance.config('SET', 'notify-keyspace-events', 'Ex');
            }
        });
    }
    catch (err) {
        logger.error(`Error al establecer conexión con servidor Redis`, err);
    }
};
exports.createRedisInstance = createRedisInstance;
const getData = (key) => __awaiter(void 0, void 0, void 0, function* () {
    if (!redisInstance) {
        logger.error('Error, no existe una instancia de cliente Redis inicializada');
        return null;
    }
    logger.debug(`Se procede a obtener datos en Redis. Llave "${key}"`);
    const data = yield redisInstance.get(key);
    logger.debug(`Los datos asociados a la llave '${key}' son: `, { data });
    return data;
});
exports.getData = getData;
const saveData = (saveConfig) => __awaiter(void 0, void 0, void 0, function* () {
    if (!redisInstance) {
        logger.error('Error, no existe una instancia de cliente Redis inicializada');
        return;
    }
    logger.debug(`Se procede a almacenar datos en Redis. Llave '${saveConfig.key}' y valor ${saveConfig.data}`);
    const pipeline = redisInstance.multi();
    pipeline.set(saveConfig.key, saveConfig.data);
    if (saveConfig.expirationSeconds) {
        logger.debug(`Se procede a setear una expiracion de ${saveConfig.expirationSeconds} segundos para la llave '${saveConfig.key}'`);
        pipeline.expire(saveConfig.key, saveConfig.expirationSeconds);
    }
    yield pipeline.exec();
    logger.debug(`Los datos asociados a la llave '${saveConfig.key}' han sido almacenados`);
});
exports.saveData = saveData;
const deleteData = (key) => __awaiter(void 0, void 0, void 0, function* () {
    if (!redisInstance) {
        logger.error('Error, no existe una instancia de cliente Redis inicializada');
        return;
    }
    logger.debug(`Se procede a eliminar la llave '${key}' en Redis`);
    yield redisInstance.del(key);
    logger.debug(`Llave '${key}' eliminada`);
});
exports.deleteData = deleteData;
//# sourceMappingURL=index.js.map