import * as redisUtil from './index';
export declare class RedisProvider {
    getData(key: string): Promise<string | null>;
    saveData(saveConfig: redisUtil.SaveData): Promise<void>;
    deleteData(key: string): Promise<void>;
}
